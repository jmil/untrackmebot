import os
from flask import Flask, request
import telebot

import constants as keys
import redirect as r

TOKEN = keys.API_KEY
bot = telebot.TeleBot(TOKEN)
server = Flask(__name__)


@bot.message_handler(commands=['start', 'help'])
def welcome_msg(message):
    bot.reply_to(message,
                 "Hi! This bot will redirect you from YouTube to an Invidious instance. Just share me a YouTube link.")


@bot.message_handler(func=lambda message: True)
def response_msg(message):
    bot.reply_to(message, r.redirect(message.text))


@server.route('/' + TOKEN, methods=['POST'])
def get_message():
    json_string = request.get_data().decode('utf-8')
    update = telebot.types.Update.de_json(json_string)
    bot.process_new_updates([update])
    return "!", 200


@server.route('/')
def webhook():
    bot.remove_webhook()
    bot.set_webhook(url='https://untrackmebot.herokuapp.com/ ' + TOKEN)
    return "!", 200


if __name__ == "__main__":
    server.run(host='0.0.0.0', port=int(os.environ.get('PORT', 5000)))
